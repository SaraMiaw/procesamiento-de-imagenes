#AUTORES:Sara Marina Haro Loor Y Danilo Alejandro Soria Maldonado
#MAIL:smharol@uce.edu.ec/dasoria@uce.edu.ec
#ULTIMA MODIFICACION :16/7/2020
#REFENRENCIA:https://docs.opencv.org/

import cv2

# Input image
input = cv2.imread('paisaje.jpg')

# Get input size
height, width = input.shape[:2]

# Desired "pixelated" size
w, h = (20, 20)

# Resize input to "pixelated" size
temp = cv2.resize(input, (w, h), interpolation=cv2.INTER_LINEAR)

# Initialize output image
output = cv2.resize(temp, (width, height), interpolation=cv2.INTER_NEAREST)

cv2.imshow('Input', input)
cv2.imshow('Output', output)

cv2.waitKey(0)