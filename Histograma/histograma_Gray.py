#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

#IMPORTAT
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
#LEER Y MOSTRAR
img = cv.imread("lenna.jfif", cv.IMREAD_GRAYSCALE)
cv.imshow("lenna.jfif", img)
#HISTOGRAMA
histograma = cv.calcHist([img],[0], None, [256],[0,256])
#MOSTRAR HISTOGRAMA
plt.plot(histograma, color="gray")
plt.xlabel("Intensidad de iluminacion")
plt.xlabel("Rango de Pixeles")
plt.show()

cv.waitKey(0)
cv.destroyAllWindows()
