#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

#IMPORTAT
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
#LEER Y MOSTRAR
img = cv.imread("marte.png")
cv.imshow("LEGENDAS", img)
#COLORES
color = ("b","g","r")
#HISTOGRAMA CON COLORES
for i, canal in enumerate(color):
    hist = cv.calcHist([img], [i], None, [256], [0, 256])
    plt.plot(hist, color = canal)
    plt.xlim([0,256])
plt.show()
cv.destroyAllWindows()