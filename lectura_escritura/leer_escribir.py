#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

#Importar libreria OPENCV
import cv2 as cv
#Funciones del SISTEMA OPERATIVO
import os
import sys
import numpy as np
####################################
initSequence=1# inicio de secuencia
numSequences=5#fin secuencia
cont_frame=0#contador auxiliar para implementar
#Directorio de lectura de imagenes (ORIGINALES)
path_RGB = "../DATABASES/17flowers/RGB"
#Directorios de escritura
path_R = "../DATABASES/17flowers/R"
path_G = "../DATABASES/17flowers/G"
path_B = "../DATABASES/17flowers/B"
path_GRAY = "../DATABASES/17flowers/GRAY"

#Contabilizar el Numero de Archivos de la Carpera de Imagenes (ORIGINALES)
total_Images = int(len(os.listdir(path_RGB)))
print( total_Images)
contador =0

#Crear un bucle que recorre  el contenido del directorio
for ns in range(initSequence,numSequences+1):
    contador = contador +1
    dir_Images=path_RGB + "/image_" + str(ns).zfill(4) + ".jpg"
    print(dir_Images)
    #Leer con OpenCV en cada imagen del directorio RGB
    imagen = cv.imread(dir_Images)
    B = cv.extractChannel(imagen, 0)
    G = cv.extractChannel(imagen, 1)
    R = cv.extractChannel(imagen, 2)
    # imagenrgb= cv.cvtColor(imagen,cv.COLOR_BGR2RGB)
    # C1 = imagen[:, :, 0]
    # C2 = imagen[:, :, 1]
    # C3 = imagen[:, :, 2]
    # cv.imshow("RGB",np.hstack([C1,C2,C3]))
    cv.imshow("R",R)
    cv.imshow("G", G)
    cv.imshow("B", B)
    #transformar una imagen RGB a escala de Grises
    #gray = cv.cvtColor(imagen,cv.COLOR_BGR2GRAY)
    # cv.imshow("Original",imagen)
    # cv.imshow("Gray", gray)
    cv.waitKey(0)
    cv.destroyAllWindows()
    cv.imwrite(path_R + "/Rimage_" + str(ns).zfill(4) + ".jpg",R )
    cv.imwrite(path_G + "/Gimage_" + str(ns).zfill(4) + ".jpg", G)
    cv.imwrite(path_B + "/Bimage_" + str(ns).zfill(4) + ".jpg", B)

