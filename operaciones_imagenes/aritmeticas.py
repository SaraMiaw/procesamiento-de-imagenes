#AUTORA:SARA MARINA HARO LOOR
#MAIL:smharol@uce.edu.ec/marsarharo3e@gmail.com
#ULTIMA MODIFICACION :2/7/2020
import cv2 as cv
# Usar OpenCV para Adición de imágenes
# cv.add
# Usar OpenCV para Mezcla de imágenes
# cv.addWeughted
# Usar OpenCV para Sustracción  de imágenes
# cv.substract
# cv.absdiff

# Leer imagen Simón Bolívar
org_simbol=cv.imread("simon_bolivar.jpg")
    #QUIERO SABER SUS DIMENSIONES (Ancho,Alto,CANALES) son 3 canales por RGB
print(org_simbol.shape)
    #VOY A CREAR LA MISMA IMAGEN PERO REDIMENCIONADA PARA PODER REALIZAR LAS OPERACIONES SOLICITADAS
img1 = cv.resize(org_simbol,(512,512))
    #VOY LEER LA MISMA IMAGEN PERO CONVERTIRLA EN ESCALA DE GRISES PARA REALIZAR OTRAS OPERACIONES
simbol_gray =cv.imread("simon_bolivar.jpg",cv.IMREAD_GRAYSCALE)
    #QUIERO SABER SUS DIMENSIONES(Ancho,Alto) El canal no aparece
print(simbol_gray.shape)
    #VOY A REDIMENSIONAR LA IMAGEN PARA PODER TRABAJAR CON ELLA
simbol_gray=cv.resize(simbol_gray,(512,512))
    #VOY A CREAR UNA NUEVA IMAGEN  EN BLANCO Y NEGRO DE LA IMAGEN ORIGINAL
(threshold,Imagenblanconegro)=cv.threshold(simbol_gray,127,255,cv.THRESH_BINARY)
    # QUIERO SABER SUS DIMENSIONES(Ancho,Alto) El canal no aparece
print(Imagenblanconegro.shape)
    # VOY A REDIMENSIONAR LA IMAGEN BLANCO Y NEGRO
Imagenblanconegro=cv.resize(Imagenblanconegro,(512,512))


# Leer imagen Torre Eiffel
img2 =cv.imread("torre_eiffel.jpg")
    #VOY LEER LA MISMA IMAGEN PERO CONVERTIRLA EN ESCALA DE GRISES PARA REALIZAR OTRAS OPERACIONES
img2GRAY =cv.imread("torre_eiffel.jpg",cv.IMREAD_GRAYSCALE)
    #QUIERO SABER SUS DIMENSIONES (Ancho,Alto,CANALES) son 3 canales por RGB
print(img2.shape)

# Leer imagen FONDO BLANCO
img3=cv.imread("fondo blanco.jpg")
    #QUIERO SABER SUS DIMENSIONES(Ancho,Alto,CANALES)
print(img3.shape)
    #VOY A REDIMENSIONAR LA IMAGEN
img3=cv.resize(img3,(512,512))
    #QUIERO SABER SUS DIMENSIONES(Ancho,Alto,CANALES)
print(img3.shape)

# Realizar operaciones solicitadas
# 1.Usar OpenCV para Adición de imágenes
    #CON LA IMAGEN DE SIMON BOLIVAR EN ESCALA DE GRISES Y LA IMAGEN DE LA TORRE EIFFEL en BLANCO Y NEGRO
dsta1 = cv.add(simbol_gray,img2GRAY);
    #CON LA IMAGEN BLANCA  Y LA IMAGEN DE LA TORRE EIFEEL EN ESCALA DE GRISES
dsta2 = cv.add(Imagenblanconegro,img2GRAY)
    #CON LA IMAGEN DE SIMON BOLIVAR Y LA TORRE EIFEEL
dsta3= cv.add(img1,img2)

# 2.Usar OpenCV para Mezcla de imágenes
dstw1=cv.addWeighted(img1,0.3,img2,0.8,0);

# 3.Usar OpenCV para Sustracción  de imágenes
    #SUSTRACCION DE IMAGENES DE LA IMAGEN 1 DE LA IMAGEN 2
dsts1=cv.subtract(img1,img2)
    #SUSTRACCION DE IMAGENES DE LA IMAGEN 2 DE LA IMAGEN 1
dsts2=cv.subtract(img2,img1)
    #SUSTRACCION DE IMAGENES DE LA IMAGEN 3 DE LA IMAGEN 2
dsts3=cv.absdiff(img3,img2)

#RESULTADOS

#cv.imshow("original",originalsm)
#cv.imshow("GRAY",img1)
#cv.imshow("BN",Imagenblanconegro)

cv.imshow("ADICION DE IMAGENES BN Y GRAYSCALE",dsta1)
cv.imshow("ADICION DE IMAGENES BLANCO Y GRAYSCALE",dsta2)
cv.imshow("ADICION DE IMAGENES RGB",dsta3)

cv.imshow("MEZCLA DE IMAGENES",dstw1)

cv.imshow("SUSTRACCION DE IMAGENES 1-2",dsts1)
cv.imshow("SUSTRACCION DE IMAGENES 2-1",dsts2)
cv.imshow("SUSTRACCION DE IMAGENES",dsts3)

cv.waitKey(0)
cv.destroyAllWindows()