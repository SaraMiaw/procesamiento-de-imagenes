#AUTORA:SARA MARINA HARO LOOR
#MAIL:smharol@uce.edu.ec/marsarharo3e@gmail.com
#ULTIMA MODIFICACION :2/7/2020
#REFENRENCIA:
import cv2 as cv
import numpy as np

#VOY A CREAR DOS IMAGENES PARA TRABAJAR
#SIMON BOLIVAR
img1= cv.imread("simon_bolivar.jpg")
   #QUIERO SABER SUS DIMENSIONES(Ancho,Alto) El canal no aparece
print(img1.shape)
img1=cv.resize(img1,(800,528))
print(img1.shape)

#MITAD DEL MUNDO
img2= cv.imread("mitad_mundo.jpg")
print(img2.shape)


# Realizar operaciones solicitadas
    #REALIZAR OPERACION LOGICA AND

AND=cv.bitwise_and(img1,img2)
    #REALIZAR OPERACION LOGICA OR

OR =cv.bitwise_or(img1,img2)
    # REALIZAR OPERACION LOGICA NOT

NOT1=cv.bitwise_not(img1)
NOT2=cv.bitwise_not(img2)
    # REALIZAR OPERACION LOGICA XOR

XOR=cv.bitwise_xor(img1,img2)
#RESULTADOS
cv.imshow("CUADRADO",img1)
cv.imshow("CIRCULO",img2)
#     #AND
cv.imshow("OPERACION LOGICA AND",AND)
#     #OR
cv.imshow("OPERACION LOGICA OR",OR)
#     #NOT
cv.imshow("OPERACION LOGICA NOT CON CUADRADO",NOT1)
cv.imshow("OPERACION LOGICA NOT CON CIRCULO",NOT2)
#     #XORT
cv.imshow("OPERACION LOGICA XOR",XOR)
#VENTANA
cv.waitKey(0)
cv.destroyAllWindows()