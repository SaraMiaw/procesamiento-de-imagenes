#AUTORA:SARA MARINA HARO LOOR
#MAIL:smharol@uce.edu.ec/marsarharo3e@gmail.com
#ULTIMA MODIFICACION :2/7/2020
#REFENRENCIA:
import cv2 as cv
import numpy as np

#VOY A CREAR DOS IMAGENES PARA TRABAJAR
#CUADRADO
img1= np.zeros((400,600),dtype=np.uint8)
img1[100:300,200:400]=255
   #QUIERO SABER SUS DIMENSIONES(Ancho,Alto) El canal no aparece
print(img1.shape)
#CIRCULO
img2= np.zeros((400,600),dtype=np.uint8)
img2=cv.circle(img2,(300,200),125,(255),-1)#ARGUMENTOS:imagen donde se dibujara,radio,color(rgb),grosor linea
print(img2.shape)


# Realizar operaciones solicitadas
    #REALIZAR OPERACION LOGICA AND
        #TABLA DE VERDAD
        #A       B        SALIDA
        # 0      0         0
        # 0      255       0
        # 255    0         0
        # 255    255       255

AND=cv.bitwise_and(img1,img2)
    #REALIZAR OPERACION LOGICA OR
        # TABLA DE VERDAD
        # A       B       SALIDA
        # 0      0         0
        # 0      255       255
        # 255    0         255
        # 255    255       255
OR =cv.bitwise_or(img1,img2)
    # REALIZAR OPERACION LOGICA NOT
        # TABLA DE VERDAD
        # A          SALIDA
        # 0           255
        # 255          0
NOT1=cv.bitwise_not(img1)
NOT2=cv.bitwise_not(img2)
    # REALIZAR OPERACION LOGICA XOR
        # TABLA DE VERDAD
        # A       B       SALIDA
        # 0      0         0
        # 0      255       255
        # 255    0         255
        # 255    255       0
XOR=cv.bitwise_xor(img1,img2)
#RESULTADOS
cv.imshow("CUADRADO",img1)
cv.imshow("CIRCULO",img2)
    #AND
cv.imshow("OPERACION LOGICA AND",AND)
    #OR
cv.imshow("OPERACION LOGICA OR",OR)
    #NOT
cv.imshow("OPERACION LOGICA NOT CON CUADRADO",NOT1)
cv.imshow("OPERACION LOGICA NOT CON CIRCULO",NOT2)
    #XORT
cv.imshow("OPERACION LOGICA XOR",XOR)
#VENTANA
cv.waitKey(0)
cv.destroyAllWindows()