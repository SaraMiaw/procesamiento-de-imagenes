#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/


import cv2
import numpy as np
from  matplotlib import pyplot as plt

img = cv2.imread('imagen_original3.jpg')
histograma_or = cv2.calcHist([img],[0], None, [256],[0,256])
img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

# equalize the histogram of the Y channel
img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])

# convert the YUV image back to RGB format
img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

hist_eql_resultado = cv2.cvtColor(img_output,cv2.COLOR_YUV2BGR)
histograma_eq = cv2.calcHist([hist_eql_resultado],[0], None, [256],[0,256])
plt.subplot(2,2,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,2),plt.plot(histograma_or,color = 'gray')
plt.title('Histograma original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(hist_eql_resultado,cmap = 'gray')
plt.title('Ecualizado'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.plot(histograma_eq,color = 'gray')
plt.title('Histograma ecualizado'), plt.xticks([]), plt.yticks([])
plt.show()

cv2.waitKey(0)
cv2.imshow('Color input image', img)
cv2.imshow('Histogram equalized', img_output)


cv2.waitKey(0)