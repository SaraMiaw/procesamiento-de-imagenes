#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageOps

img = cv2.imread('imagen_original3.jpg',1)
hist,bins = np.histogram(img.flatten(),256,[0,256])
cdf = hist.cumsum()
cdf_normalized = cdf * hist.max()/ cdf.max()

#Enmascara los valores iguales a cero
cdf_m = np.ma.masked_equal(cdf,0)
#Aplica la transformación de ecualización
cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
#Rellena los valores previamente enmascarados con ceros
cdf = np.ma.filled(cdf_m,0).astype('uint8')
#Aplica la ecualización a los píxeles de la imagen original
img2 = cdf[img]

#Grafica la imagen resultante de aplicar la ecualización del histograma
cv2.imshow('image1',img)
cv2.imshow('image2',img2)

color =("b","g","r")
for i, canal in enumerate(color):
    hist1 = cv2.calcHist([img],[i],None,[256],[0,256])
    plt.plot(hist1, color= canal)
    plt.xlim([0,256])
plt.show()
for j, canal in enumerate(color):
    hist2 = cv2.calcHist([img2], [j], None, [256], [0, 256])
    plt.plot(hist2, color= canal)
    plt.xlim([0,256])
plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()

