#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

#importar librerias
import cv2 as cv
import numpy as np
from  matplotlib import pyplot as plt

img = cv.imread("imagen_original.png")
#histograma
histograma_or = cv.calcHist([img],[0], None, [256],[0,256])
img_to_eql = cv.cvtColor(img,cv.COLOR_BGR2YUV)
img_to_eql[:,:,0] = cv.equalizeHist(img_to_eql[:,:,0])
hist_eql_resultado = cv.cvtColor(img_to_eql,cv.COLOR_YUV2BGR)
histograma_eq = cv.calcHist([hist_eql_resultado],[0], None, [256],[0,256])
plt.subplot(2,2,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
#mostrar histograma
#plt.subplot(2,2,2),plt.imshow(histograma_or,cmap = 'gray')
plt.subplot(2,2,2),plt.plot(histograma_or,color = 'gray')
plt.title('Histograma original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(hist_eql_resultado,cmap = 'gray')
plt.title('Ecualizado'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.plot(histograma_eq,color = 'gray')
plt.title('Histograma ecualizado'), plt.xticks([]), plt.yticks([])
plt.show()
cv.waitKey(0)
cv.destroyAllWindows()
cv.imwrite("imagen_resultado.png",hist_eql_resultado)