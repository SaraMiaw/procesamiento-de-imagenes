import random

def ganador(fingers):
   aleatorio = random.randint(0, 3)
   machine = "piedra"
   player="piedra"
   if fingers == 0:
    player = "piedra"
   # en el caso que tengamos dos dedos, seleccionamos tijera (Nota: aclarar cuales dedos)
   elif fingers == 2:
    player = "tijera"
   # cuando tengamos los 5 dedos, es decir la mano abierta, nosotros como usuario seleccionamos papel
   elif fingers == 5:
    player = "papel"

   if aleatorio == 0:
    machine = "piedra"  # eleccion de la computadora
   # Si el numero aleatorio es 1 sera papel
   elif aleatorio == 1:
    machine = "papel"
   # Si el numero aleatorio es dos sera tijera
   elif aleatorio == 2:
    machine = "tijera"


   if player == machine:
    return "PC eligio: " +machine+ "Usted eligio: "+player+" -EMPATE"
   # -----------------------------
   if player == "piedra" and machine == "papel":
    return "PC eligio: " +machine+ "Usted eligio: "+player+"-La maquina GANA!"
   if player == "piedra" and machine == "tijera":
    return "PC eligio: " +machine+ "Usted eligio: "+player+" -GANASTE"
   # -----------------------------
   if player == "papel" and machine == "piedra":
    return "PC eligio: " +machine+ "Usted eligio: "+player+" -GANASTE!"
   if player == "papel" and machine == "tijera":
    return "PC eligio: " +machine+ "Usted eligio: "+player+" -La maquina gana!"
   # -----------------------------
   if player == "tijera" and machine == "piedra":
    return "PC eligio: " +machine+ "Usted eligio: "+player+" -La maquina gana!"
   if player == "tijera" and machine == "papel":
    return "PC eligio: " +machine+ "Usted eligio: "+player+" -GANASTE!"