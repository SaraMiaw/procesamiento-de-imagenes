#AUTORES:Sara Marina Haro Loor,Joana Estefania Nicolalde Perugachi ,Victor Daniel Maldonado Nolivos, Danilo Alejandro Soria Maldonado
#MAIL:smharol@uce.edu.ec/jenicolaldep@uce.edu.ec/vdmaldonado@uce.edu.ec/dasoria@uce.edu.ec/
#ULTIMA MODIFICACION :28/07/2020 --->Inicio de proyecto, implementar contador de dedos
#REFENRENCIA:https://omes-va.com/,https://docs.opencv.org/

                                #!!!!!!!!!NOTA IMPORTANTE!!!!!!!!!!!!!#
        #ES IMPORTANTE CONTAR CON UN FONDO DE COLOR QUE AYUEDE A LA DISTINCION DE LA MANO PARA SU DIFERENCIACION#
                        #LOS VALORES DE DISTANCIA PARA CADA DEDO PUEDEN VARIAR#

#IMPORTACION
import cv2 as cv
import numpy as np
import imutils# Utilizamos imutils para REDIMENSIONAR LOS FOTOGRAMAS, SE DEBE INSTALAR "pip install imutils"
#PARA REPRODUCIR SONIDO
from gtts import gTTS
from playsound import playsound
import  threading

#INICIO DEL PROGRAMA
#Video Stream con la camara de la computadora
cap = cv.VideoCapture(0)
#Creamos una variable  que almacene el fondo y nos ayude a realizar sustraccion
bg=None

# COLORES PARA VISUALIZACIÓN: Los usaremos unicamente al momento de la visualizacion de distintos datos
color_start = (204,204,0)
color_end = (204,0,204)
color_far = (255,0,0)
color_start_far = (204,204,0)
color_far_end = (204,0,204)
color_start_end = (0,255,255)
color_contorno = (0,255,0)
color_ymin = (0,130,255) # Punto más alto del contorno
color_angulo = (0,255,255)
color_d = (0,255,255)
color_fingers = (0,255,255)

#VARIABLES QUE HABLEN
uno = gTTS('Un dedo levantado', lang='es-us')
uno.save("uno.mp3")
dos = gTTS('dos levantados', lang='es-us')
dos .save("dos.mp3")
tres = gTTS('tres dedos levantados', lang='es-us')
tres.save("tres.mp3")
nd = gTTS('no definido', lang='es-us')
nd.save("nd.mp3")

#SONIDOS
def Repsound(numero):
   if numero == 1:
    playsound("uno.mp3")
   elif numero == 2:
       playsound("dos.mp3")
   elif numero == 3:
       playsound("tres.mp3")
   elif numero == 0:
       playsound("nd.mp3")
#HILOS


#ENTRADA BUCLE DE VISUALIZACION
while True:
  ret, frame = cap.read()
  if ret == False: break
  #Redimensionar la imagen para que tenga un ancho de 640
  frame = imutils.resize(frame,width=640)
  #Efecto de espejo
  frame = cv.flip(frame,1)
  #FrameAux es una copia del frame que nos ayuda a capturar el fondo de la escena
  frameAux = frame.copy()

  if bg is not None:
      # PARA SABER QUE ESTAMOS VISUALIZANDO ESA IMAGEN
      #cv.imshow('FONDO',bg)
      #CREAMOS UNA REGIO DE INTERES, Tomamos una posicion de la imagen contenida en frame para trabajar sobre esta
      ROI=frame[50:300,380:600]
      #VISUALIZAR EL AREA, aparecera el rectangulo cuando sepamos que se esta tomando la imagen del fondo
      cv.rectangle(frame,(380-2,50-2),(600+2,300+2),color_fingers,1)
      #Transformamos la region de interes a escala de grises
      grayROI=cv.cvtColor(ROI,cv.COLOR_BGR2GRAY)
    #GUARDAR EL FONDO DE LA REGION DE INTERES
      bgROI=bg[50:300,380:600]
      #VISUALIZACION
      #cv.imshow('REGION DE INTERES',ROI)
      #cv.imshow('REGION DE INTERES GRISES', grayROI)
      #cv.imshow(' BACK REGION DE INTERES', bgROI)

      #SUSTRACCION
      dif=cv.absdiff(grayROI,bgROI)#PRIMER PLANO  Y EL FONDO
      #UMBRALIZACION SIMPLE la region en blanco representara la mano y la region en negro el fondo#ES LA IMAGEN MAS IMPORTANTE
      _, th = cv.threshold(dif, 40, 255, cv.THRESH_BINARY)
      #para mejorar el corte
      th=cv.medianBlur(th,7)
      #VISUALIZACION
      #cv.imshow('DIFERENCIA', dif)
      cv.imshow('UMBRALIZACION', th)
        #DETECTAR LOS CONTORNOS
      cnts, hierarchy = cv.findContours(th, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
      #ORDENARLOS DE MAYOR A MENOR SEGUN EL AREA,quiero obtener el contorno mas grande
      cnts=sorted(cnts,key=cv.contourArea,reverse=True)[:1]
      cv.drawContours(ROI,cnts,0,(0,255,0),1)

      for cnt in cnts:
          # MOMENTS es un promedio ponderado particular de las intensidades de píxeles de la imagen,
          # con la ayuda de la cual podemos encontrar algunas propiedades específicas de una imagen,
          # como el radio,
          # el área, el centroide, etc
          #CENTROIDE
          M=cv.moments(cnt)
          if M["m00"]==0:M["m00"]=1
          x = int(M["m10"] / M["m00"])
          y = int(M['m01'] / M['m00'])
          #DIBUJAREMOS LOS PUNTOS CENTRALES DEL CONTORNO
          cv.circle(ROI,(x,y),5,(0,255,0),-1)

          #EN EL EJE Y EL MINIMO PARA ENCONTRAR EL PUNTO MAS ALTO DEL CONTORNO
          #ymin[0] , porque el la posicion 0 se almacenaran las coordenadas en donde esta presente 'y ' minimo
          ymin=cnt.min(axis=1)
          cv.circle(ROI,tuple(ymin[0]),5,(color_ymin),-1)

          #CONVEX HULL nos ayudara a determinar los defectos de convexidad en el contorno
          #PARAMETROS
          #points :contorno encontrado
          #return points por defecto es TRUE, devuelve las coordenadas del casco convexo
          #si es false devuelve los limites del casco convexo, ayudara a determinar los defectos de convexidad

          hull1=cv.convexHull(cnt)
          cv.drawContours(ROI,[hull1],0,color_contorno,2)
          #LOS ESPACIOS VACIOS ENTRE LOS DEDOS SON ENTONCES DEFECTOS DE CONVEXIDAD ,USAREMOS CONVEXITYDEFECTS
            #Una vez calculado el casco convexo y devuelva FALSE en return poins , aplicaremos esta funcion
          #para obtener los defectos de convexidad
          #PARAMETROS
          #Contorno de entrada,
          #Casco convexo
          #ESTA FUNCION DEVOLVERA UN ARRAY CON
          #[PUNTO INICIAL,PUNTO FINAL,PUNTO MAS ALEJADO,DISTANCIA APROXIMADA AL PUNTO MAS ALEJADO]
          hull2 = cv.convexHull(cnt,returnPoints=False)
          defects=cv.convexityDefects(cnt,hull2)
          #Desempaquetamos los defectos, en el caso de que existan defectos convexos
          if defects is not None:
              #Visualizar cada numero en cada dedo
              inicio =[]
              fin=[]
              #contador de dedos
              fingers=0

              for i in range (defects.shape[0]):
                  #AQUI 'desempaquetamos los puntos'
                  s,e,f,d=defects[i,0]
                  start=cnt[s][0]
                  end=cnt[e][0]
                  far=cnt[f][0]
                #ANGULOS FORMADOS POR LOS DEDOS
                  #PUNTOS DEL TRIANGULO
                  a=np.linalg.norm(far-end)#INICIAL
                  b=np.linalg.norm(far-start)#FINAL
                  c=np.linalg.norm(start-end)#MEDIO--->ANGULO
                  angulo=np.arccos((np.power(a,2)+np.power(b,2)-np.power(c,2))/(2*a*b))#ENTREGA EN RADIANES
                  #TRANSOFMRAMOS A GRADOS
                  angulo=np.degrees(angulo)
                  angulo=int(angulo)
                  #El ANGULO mas grande se encuentra entre el pulgar e indice , de aprox 90


                  # SE TIENE MUCHOS PUNTOS POR LO QUE ES NECESARIO FILTRARLOS PARA OBTENER LOS CORRESPONDIENTES DEDOS

                  # El primer aspecto a considerar es la distancia entre el punto incial  y el final
                  # Si esa distancia es muy pequena entonces descartamos ese defecto convexo
                  # lA FUNCION LINALG.norm de numpy ayuda a calcular la distancia entre dos puntos
                  #Debemos tomar en cuenta que al doblar los dedos puede dar angulos mayores a 90 y esto podria generar problemas
                  if np.linalg.norm(start-end)>20 and d>12000 and angulo <90:
                      #TODOS LOS PUNTOS QUE SI PASAN , LOS QUEREMOS ALMACENAR PARA VISUALIZAR CADA DEDO LEVANTADO
                      inicio.append(start)
                      fin.append(end)
                      # Visualizacion de cada uno de ellos
                      #cv.putText(ROI, '{}'.format(d), tuple(far), 1, 1.5, color_d,cv.LINE_4)  # d  es DISTANCIA A NO UN PUNTO, lo veremos en el punto mas alejado
                      cv.putText(ROI, '{}'.format(angulo), tuple(far), 1, 1.5, color_angulo, cv.LINE_4)#ANGULOS APROXIMADOS ENTRE DEDOS
                      cv.circle(ROI, tuple(start), 5, color_start, 2)  # CIAN
                      cv.circle(ROI, tuple(end), 5, color_end, 2)  # MORADO
                      cv.circle(ROI, tuple(far), 7, color_far, -1)  # AZUL
        #VAMOS A CONTAR LOS DEDOS
              if len(inicio) ==0: #Osea que no se a almacenado nada
                  minY=np.linalg.norm(ymin[0]-[x,y])#DISTANCIA ENTRE DOS PUNTOS, YMIN ES EL PUNTO MAS ALTO,[X,Y] EL CENTRO DEL CONTORNO

                  print(minY)
                  if minY >=80:
                      fingers=fingers+1
                      #playsound("uno.mp3")
                      cv.putText(ROI, '{}'.format(fingers), tuple(ymin[0]), 1, 1, color_fingers,1, cv.LINE_4)
                #RECORREMOS
              for i in range(len(inicio)):
                  fingers=fingers+1
                  cv.putText(ROI, '{}'.format(fingers), tuple(inicio[i]), 1, 1, color_fingers, 1, cv.LINE_4)#SE PERDERDERA EL ULTIMO VALOR

                  if i== len(inicio)-1:
                      fingers = fingers + 1
                      cv.putText(ROI, '{}'.format(fingers), tuple(fin[i]), 1, 1, color_fingers, 1, cv.LINE_4)
             #MOSTRAREMOS EL NUMERO DE DEDOS LEVANTADOS SOBRE EL RECTANGULO DIBUJADO
              cv.putText(frame, '{}'.format(fingers), (390,45), 1, 4, color_fingers, 1, cv.LINE_4)
              #hilo1 = threading.Thread(target=Repsound(fingers))
              #hilo1.start()
              #hilo1.join()


  # MOSTRAMOS LAS IMAGENES QUE SE ESTAN CAPTURANDO EN ESE MOMENTO
  cv.imshow('Frame', frame)


  # Pido que espere un momento entre fotogramas (20) para poder visualizar de mejor manera EL VIDEO
  k = cv.waitKey(20)
  #Si presionamos i almacenamos el fondo de la escena al presionar la letra i
  if k==ord('i'):
      bg=cv.cvtColor(frameAux,cv.COLOR_BGR2GRAY)
  #Cuando se presione ESC saldra del ciclo
  if k==27:
      break


#FIN BUCLE

cap.release()#DESPLEGAR
cv.destroyAllWindows()