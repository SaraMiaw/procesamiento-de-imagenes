"""
    Convertir texto a voz (TTS) con
    Python usando gTTS
    Ejemplo 4: Escribir hola mundo en un archivo, en idioma español
    y después reproducirlo
    @author parzibyte

    pip install gTTS
    pip install playsound
"""
from gtts import gTTS
from playsound import playsound

tts = gTTS('Hola mundo. Estamos convirtiendo texto a voz con Python.', lang='es-us')
# Nota: podríamos llamar directamente a save
tts.save("NOMBRE_ARCHIVO.mp3")
playsound("NOMBRE_ARCHIVO.mp3")