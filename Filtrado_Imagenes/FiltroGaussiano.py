
#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/,https://github.com/opencv/opencv
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('img_ruido_pimienta.jpg')
#FILTRO GAUSSIANO
#utiliza desviacion estandar

plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(gauss),plt.title('FILTRO GAUSSIANO')
plt.xticks([]), plt.yticks([])
plt.show()