#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('img_ruido_pimienta.jpg')
#Crea el kernel 5x5 / 25
kernel = np.ones((5,5),np.float32)/25
#Filtra la imagen utilizando el kernel anterior
dst = cv.filter2D(img,-1,kernel)
plt.subplot(141),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(142),plt.imshow(dst),plt.title('Promediada')
plt.xticks([]), plt.yticks([])
dst2=cv.filter2D(dst,-1,kernel)
plt.subplot(143),plt.imshow(dst2),plt.title('Promediada 2')
plt.xticks([]), plt.yticks([])
kernel2 = np.ones((3,3),np.float32)/9
dst3=cv.filter2D(dst,-1,kernel2)
plt.subplot(144),plt.imshow(dst3),plt.title('Promediada 3 ')
plt.xticks([]), plt.yticks([])
plt.show()