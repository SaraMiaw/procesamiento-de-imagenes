
#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

from __future__ import division
from itertools import islice, count
from collections import Counter
from math import log10
from random import randint

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
img=cv.imread("lena.jpg")
img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
img2=cv.resize(img,(10,10))

#matriz = np.array(img2).flatten()
matriz = np.array(cv.imread("lena.jpg")).flatten()
print(matriz)
#b,g,r=cv.split(img)
#img=cv.merge((b,g,r))
listab = []
b=img2[:,:,0]
g=img2[:,:,1]
r=img2[:,:,2]
plt.subplot(221),plt.imshow(img2),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(b),plt.title('B')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(g),plt.title('G')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(r),plt.title('R')
plt.xticks([]), plt.yticks([])
#plt.show()

#print("Canal B",b)
#print("Canal G",g)
#print("Canal R",r)
arr1 = np.array(b).flatten()
arr2 = np.array(g).flatten()
arr3 = np.array(g).flatten()

arr4 = np.append(arr1,arr2)
arr5 = np.append(arr4,arr3)
lista1 = list(arr5)
#print(lista1)
#print(arr5)



#print(arrayB)


expected = [log10(1 + 1 / d) for d in range(1, 10)]


def fib():
    a, b = 1, 1
    while True:
        yield a
        a, b = b, a + b


# powers of 3 as a test sequence
def power_of_threes():
    return (3 ** k for k in count(0))


def heads(s):
    for a in s: yield int(str(a)[0])


def show_dist(title, s):
    c = Counter(s)
    size = sum(c.values())
    res = [c[d] / size for d in range(1, 10)]

    print("\n%s Benfords deviation" % title)
    i = 0
    for r, e in zip(res, expected):
        i = i+1
        print("%5.1f%% %5.1f%%  %5.1f%%" % (r * 100., e * 100., abs(r - e) * 100.))


def rand1000():
    while True: yield randint(1, 9999)
#maximo = len(arr5)
maximo = len(lista1)
def randImage():
    i = 0
    #while True and i < maximo: yield arr5[i]
    while True and i < maximo: yield lista1[i]
    i = i+1



if __name__ == '__main__':
    show_dist("fibbed", islice(heads(fib()), maximo))
    show_dist("threes", islice(heads(power_of_threes()), maximo))

    # just to show that not all kind-of-random sets behave like that
    show_dist("random", islice(heads(rand1000()), maximo))
    show_dist("imageB", islice(heads(randImage()), maximo))