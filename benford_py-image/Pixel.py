
#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
img=cv.imread("lena.jpg")
img = cv.cvtColor(img, cv.COLOR_RGB2BGR)
img2=cv.resize(img,(10,10))
#b,g,r=cv.split(img)
#img=cv.merge((b,g,r))
listab = []
b=img2[:,:,0]
g=img2[:,:,1]
r=img2[:,:,2]
plt.subplot(221),plt.imshow(img2),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(b),plt.title('B')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(g),plt.title('G')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(r),plt.title('R')
plt.xticks([]), plt.yticks([])
#plt.show()

print("Canal B",b)
#print("Canal G",g)
#print("Canal R",r)

for i in b:
    listab.append(i)

arrayB = np.array(listab)
arrayB=arrayB.flatten()
arrayB.sort()
print(arrayB)

