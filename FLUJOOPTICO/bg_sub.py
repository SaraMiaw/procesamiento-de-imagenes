#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/,https://answers.opencv.org/questions/,https://github.com/opencv/opencv

from __future__ import print_function
import cv2 as cv
import argparse

parser = argparse.ArgumentParser(description='This program shows how to use background subtraction methods provided by \
                                              OpenCV. You can process both videos and images.')
parser.add_argument('--input', type=str, help='Path to a video or a sequence of image.', default='vtest.avi')
parser.add_argument('--algo', type=str, help='Background subtraction method (KNN, MOG2).', default='MOG2')
args = parser.parse_args()

## [create]
#create Background Subtractor objects
if args.algo == 'MOG2':
    backSub = cv.createBackgroundSubtractorMOG2()
else:
    backSub = cv.createBackgroundSubtractorKNN()
## [create]

## [capture]
# Método por argumentos
#capture = cv.VideoCapture(cv.samples.findFileOrKeep(args.input))
# Método por lectura de archivos
capture = cv.VideoCapture(cv.samples.findFile("47300-58400.mp4"))
if not capture.isOpened:
    print('Unable to open: ' + args.input)
    exit(0)
## [capture]
#salida = cv.VideoWriter('./video/BEHAVE-bg_sub.avi',cv.VideoWriter_fourcc(*'XVID'),20.0,(640,480))
salida = cv.VideoWriter( './video/BEHAVE-bg_sub.avi', cv.VideoWriter_fourcc(*'XVID'), 35.0, (640,480),isColor = False )
cont=0
while True:
    cont=cont+1
    ret, frame = capture.read()
    if frame is None:
        break

    ## [apply]
    #update the background model
    fgMask = backSub.apply(frame)
    ## [apply]

    ## [display_frame_number]
    #get the frame number and write it on the current frame
    cv.rectangle(frame, (10, 2), (100,20), (255,255,255), -1)
    cv.putText(frame, str(capture.get(cv.CAP_PROP_POS_FRAMES)), (15, 15),
               cv.FONT_HERSHEY_SIMPLEX, 0.5 , (0,0,0))
    ## [display_frame_number]

    ## [show]
    #show the current frame and the fg masks
    cv.imwrite("./background/BEHAVE-frame-%d.jpg" % cont, fgMask)
    # video
    salida.write(fgMask)
    cv.imshow('Frame', frame)
    cv.imshow('FG Mask', fgMask)

    ## [show]

    keyboard = cv.waitKey(30)
    if keyboard == 'q' or keyboard == 27:
        break