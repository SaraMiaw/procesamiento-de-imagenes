#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/,https://answers.opencv.org/questions/,https://github.com/opencv/opencv

from __future__ import print_function
import cv2 as cv
import argparse
import numpy as np

parser = argparse.ArgumentParser(description='This program shows how to use background subtraction methods provided by \
                                              OpenCV. You can process both videos and images.')
parser.add_argument('--input', type=str, help='Path to a video or a sequence of image.', default='vtest.avi')
parser.add_argument('--algo', type=str, help='Background subtraction method (KNN, MOG2).', default='MOG2')
args = parser.parse_args()

## [create]
#create Background Subtractor objects
if args.algo == 'MOG2':
    backSub = cv.createBackgroundSubtractorMOG2()
else:
    backSub = cv.createBackgroundSubtractorKNN()
## [create]

## [capture]
# Método por argumentos
#capture = cv.VideoCapture(cv.samples.findFileOrKeep(args.input))
# Método por lectura de archivos
capture = cv.VideoCapture(cv.samples.findFile("47300-58400.mp4"))
if not capture.isOpened:
    print('Unable to open: ' + args.input)
    exit(0)
## [capture]
#salida = cv.VideoWriter('./video/BEHAVE-bg_sub.avi',cv.VideoWriter_fourcc(*'XVID'),20.0,(640,480))
salida = cv.VideoWriter( './video/BEHAVE-fg_ext.avi', cv.VideoWriter_fourcc(*'XVID'), 35.0, (640,480) )
cont=0
while True:
    cont=cont+1
    ret, frame = capture.read()
    if frame is None:
        break
    #---------------------
    mask = np.zeros(frame.shape[:2], np.uint8)
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    rect = (50, 50, 450, 290)
    cv.grabCut(frame, mask, rect, bgdModel, fgdModel, 5, cv.GC_INIT_WITH_RECT)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype('uint8')
    frame = frame * mask2[:, :, np.newaxis]

    #---------------------
    #show the current frame and the fg masks
    cv.imwrite("./foreground/BEHAVE-frame-%d.jpg" % cont, frame)
    print("Contador: ",cont)
    # video
    salida.write(frame)
    cv.imshow('Frame', frame)
    #cv.imshow('FG Mask', fgMask)

    ## [show]

    keyboard = cv.waitKey(30)
    if keyboard == 'q' or keyboard == 27:
        break