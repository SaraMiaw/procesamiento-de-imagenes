#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/,https://answers.opencv.org/questions/,https://github.com/opencv/opencv

import numpy as np
import cv2 as cv
#cap = cv.VideoCapture(cv.samples.findFile("vtest.avi"))
#cap = cv.VideoCapture(cv.samples.findFile("47300-58400.mp4"))
cap=cv.VideoCapture(0)
#cap = cv.VideoCapture(cv.samples.findFile("./video/BEHAVE-optical_flow.avi"))
# salida de video
salida = cv.VideoWriter('./video/BEHAVE-optical_flow_trajectoryYYY.avi',cv.VideoWriter_fourcc(*'XVID'),10.0,(640,480))
ret, frame1 = cap.read()
prvs = cv.cvtColor(frame1,cv.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[...,1] = 255
cont=0
while(1):
    cont=cont+1
    ret, frame2 = cap.read()
    next = cv.cvtColor(frame2,cv.COLOR_BGR2GRAY)
    flow = cv.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    mag, ang = cv.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv.normalize(mag,None,0,255,cv.NORM_MINMAX)
    bgr = cv.cvtColor(hsv,cv.COLOR_HSV2BGR)
    cv.imwrite("./optical_flow/A-BEHAVE-frame-traj-YYY%d.jpg" % cont, bgr)
    # video
    salida.write(bgr)
    cv.imshow('frame2',bgr)
    k = cv.waitKey(30) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv.imwrite('opticalfb.png',frame2)
        cv.imwrite('opticalhsv.png',bgr)
    prvs = next