#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/
import cv2
import numpy as np


cap = cv2.VideoCapture(0)

while True:

    ret, frame = cap.read()
    if ret == False: break
    #convertir a hvs
    #hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    (threshold, Imagenblanconegro) = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

    lower_red = np.array([30, 150, 50])
    upper_red = np.array([255, 255, 180])

    #mask = cv2.inRange(hsv, lower_red, upper_red)
    #res = cv2.bitwise_and(frame, frame)

    kernel = np.ones((5, 5), np.uint8)#MATRIZ DE PIXEL DE REFERENCIA
    erosion = cv2.erode(Imagenblanconegro, kernel, iterations=1)
    dilation = cv2.dilate(Imagenblanconegro, kernel, iterations=1)

    cv2.imshow('Original', frame)
    cv2.imshow('Mascara', Imagenblanconegro)
    cv2.imshow('Erosion', erosion)
    cv2.imshow('Dilatacion', dilation)


    if cv2.waitKey(30) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
