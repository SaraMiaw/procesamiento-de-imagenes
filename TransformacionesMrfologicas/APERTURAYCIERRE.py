#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/
import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:

    ret, frame = cap.read()

    if ret == False: break

    #hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    (threshold, Imagenblanconegro) = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    lower_red = np.array([30, 150, 50])
    upper_red = np.array([255, 255, 180])

    #mask = cv2.inRange(hsv, lower_red, upper_red)
    #res = cv2.bitwise_and(frame, frame)

    kernel = np.ones((4, 4), np.uint8)

    opening = cv2.morphologyEx(Imagenblanconegro, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(Imagenblanconegro, cv2.MORPH_CLOSE, kernel)


    cv2.imshow('Original', frame)
    cv2.imshow('Mascara', Imagenblanconegro)
    cv2.imshow('APERTURA', opening)
    cv2.imshow('CIERRE', closing)

    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
