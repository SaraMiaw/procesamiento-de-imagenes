#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

#Importar libreria opencv
import cv2 as cv

#Leer imagen
imagen = cv.imread("./Imagenes/gato.jpg")
#Visualizar la imagen
cv.imshow("mi gato",imagen)
#Esperar tecla
cv.waitKey(0)
