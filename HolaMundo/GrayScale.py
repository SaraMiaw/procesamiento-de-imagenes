#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import cv2 as cv
import sys
#Leer Directorios externos
hada = cv.imread(cv.samples.findFile("../MiImagen/Hada1.jpg"),cv.IMREAD_GRAYSCALE)
#hada = cv.imread(cv.samples.findFile("C:/Users/marsa/MiImagen/Hada1.jpg"))
#if hada is None:
   # sys.exit("No se puede leer  la imagen ")
cv.imshow('Hada del bosque', hada)
# cargar el archivo PNG indicado
img = cv.imread("./Imagenes/gato.jpg", cv.IMREAD_GRAYSCALE)
imagen = cv.imread("./Imagenes/gato.jpg")
# mostrar la imagen en una ventana
cv.imshow('MI GATO SIN COLORES', img)
cv.imshow('MI GATO con COLORES', imagen)
# guardar la imagen en formato JPG
cv.imwrite("./Imagenes/gato-gray.jpg", img)
cv.imwrite("../MiImagen/Hada-gray.jpg",hada)
# esperar hasta que se presiona una tecla
cv.waitKey(0)
#Para liberar memoria cerrando ventanas
cv.destroyAllWindows()
