#AUTORES:Sara Marina Haro Loor Y Danilo Alejandro Soria Maldonado
#MAIL:smharol@uce.edu.ec/dasoria@uce.edu.ec
#ULTIMA MODIFICACION :16/7/2020 COMBIO NOMBRES VARIABLES
#REFENRENCIA:https://docs.opencv.org/

#IMPORTAMOS LAS LIBRERIAS
import cv2
import numpy as np

#CREAMOS UNA FUNCION PARA DIBUJAR LOS CONTORNOS DE COLOR DETECTADO
def dibujar(mask,color):#RECIBE COMO PARAMETRO UNA MASCARA Y EL COLOR CON QUE DIBUJAREMOS EL CONTORNO
    # Usar findContours para encontrar el número de contornos en la imagen y encontrar el centro de cada uno de ellos.
    contornos, hierarchy = cv2.findContours( mask ,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for c in contornos:
        area=cv2.contourArea(c)
        if area > 3000:#DESCARTAR AQUELLAS AREAS IRRELEVANTES
            #es un promedio ponderado particular de las intensidades de píxeles de la imagen,
            # con la ayuda de la cual podemos encontrar algunas propiedades específicas de una imagen,
            # como el radio,
            # el área, el centroide, etc.
            M = cv2.moments(c)
            if(M["m00"]==0):M["m00"]=1
            x = int(M["m10"]/M["m00"])
            y = int(M['m01'] / M['m00'])
            nuevoContorno = cv2.convexHull(c)
            cv2.circle(frame,(x,y),7,(0,255,0),-1)
            cv2.putText(frame,'{},{}'.format(x,y),(x+10,y),font,0.75,(0,255,0),1,cv2.LINE_AA)
            cv2.drawContours(frame,[nuevoContorno],0,color,3)

captura = cv2.VideoCapture(0) #Enciente el dispositivo de video, el argumento indica el indice, en este caso 0 corresponde a la webcam
#RANGOS DE COLOR
#RANGO ROJO
redBajo1=np.array([0,100,20],np.uint8)
redAlto1=np.array([8,255,255],np.uint8)

redBajo2=np.array([175,100,20],np.uint8)
redAlto2=np.array([179,255,255],np.uint8)
#RANGO AMARILLO
amarilloBajo=np.array([15,100,20],np.uint8)
amarilloAlto=np.array([45,255,255],np.uint8)
#RANGO AZUL
azulBajo=np.array([100,100,20],np.uint8)
azulAlto=np.array([125,255,255],np.uint8)

font = cv2.FONT_HERSHEY_SIMPLEX#ESCOGEMOS UNA FUENTE

while(captura.isOpened()):#Mientras el dispositivo de video este encendido
    ret,frame=captura.read()#De la variable captura, lee fotograma por fotograma
    if ret==True:
        #Convertimos a  HSV
        frameHSV=cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        #Creamos las diferentes mascaras con respecto a los rangos ya determinados
        maskAzul=cv2.inRange(frameHSV,azulBajo,azulAlto)
        maskAmarillo=cv2.inRange(frameHSV,amarilloBajo,amarilloAlto)
        #POR TEORIA: se debe crear tres mascaras para el rojo
        maskRed1=cv2.inRange(frameHSV,redBajo1,redAlto1)
        maskRed2 = cv2.inRange(frameHSV, redBajo2, redAlto2)
        maskRed = cv2.add(maskRed1,maskRed2)
        #Dibujamos las mascaras
        dibujar(maskAzul,(255,0,0))
        dibujar(maskRed,(0,0,255))
        dibujar(maskAmarillo,(0,255,255))
        cv2.imshow('video',frame)#EXPONEMOS LA IMAGEN

        #ESPERAR  LA TECLA S
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break

captura.release()#DESPLEGAMOS

cv2.destroyAllWindows()