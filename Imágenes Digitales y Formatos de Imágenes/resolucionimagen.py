#AUTORES:Sara Marina Haro Loor Y Danilo Alejandro Soria Maldonado
#MAIL:smharol@uce.edu.ec/dasoria@uce.edu.ec
#ULTIMA MODIFICACION :16/7/2020 COMBIO NOMBRES VARIABLES
#REFENRENCIA:https://docs.opencv.org/

#IMPORTAMOS LAS LIBRERIAS
import cv2
import numpy as np

#Enciente el dispositivo de video, el argumento indica el indice, en este caso 0 corresponde a la webcam
cap = cv2.VideoCapture(0)
#VARIABLES QUE USAEREMOS
#DETERMINAMOS CUAL DESEAMOS SEA EL VALOR DE PIXEL
w, h = (60, 60)#tamaño de pixel
#MATRIZ DE TRASLACION Y ROTACION
M=np.float32([[1,0,-180],[0,1,-100]])
#KERNEL PARA EL FILTRO
kernel = np.ones((11,11),np.float32)/25

#Definimos una funcion que nos reescale el tamaño de una ventana
def rescale_frame(frame, percent):#como parametrorecibimos el frame (original) y el porcentaje de escalado
    width = int(frame.shape[1] * percent/ 100)
    height = int(frame.shape[0] * percent/ 100)
    dim = (width, height)
    return cv2.resize(frame, dim, interpolation =cv2.INTER_AREA)#INTER_AREA  : remuestreo utilizando la relación de área de píxeles.
    # Puede ser un método preferido para la reduccion de imágenes,
    # ya que proporciona resultados sin muaré(patrón de interferencia,
    # se forma cuando se superponen dos rejillas de líneas, ya sean rectas o curva).
    # Pero cuando la imagen se amplía, es similar al metodo INTER_NEAREST .

while (cap.isOpened()):#Mientras el dispositivo de video este encendido
    rect, frame = cap.read()#De la variable captura, lee fotograma por fotograma
    if rect==True:
        frameA = rescale_frame(frame, percent=100)
        cv2.imshow('frameA', frameA)
        #PARA REESCALAR UNA IMAGEN
        frameB = rescale_frame(frameA, percent=50)
        #PARA PIXELAR UNA IMAGEN
        #temp = cv2.resize(frameA, (w, h), interpolation=cv2.INTER_LINEAR)#una interpolación bilineal (utilizada por defecto)
        #frameB = cv2.resize(temp, (600, 500), interpolation=cv2.INTER_NEAREST)# una interpolación del vecino más cercano
        #PARA VER UNA IMAGEN EN NEGATIVO
        #frameB= cv2.bitwise_not(frameA)
        #IMAGEN ROTADA
        #M = cv2.getRotationMatrix2D((frameB.shape[1],frameB.shape[0]),180,1)
        #frameB=cv2.warpAffine(frame,M,(600,500))
        #IMAGEN APLICADA UN FILTRO
        #frameB = cv2.GaussianBlur(frameB, (5, 5), 0)
        #frameB = cv2.filter2D(frameB, -1, kernel)


        cv2.imshow('frameB', frameB)

        # ESPERAR  LA TECLA S
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break


cap.release()#DESPLEGAMOS
cv2.destroyAllWindows()