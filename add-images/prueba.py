#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import cv2
from Negativo import funcionNegativo
from matplotlib import pyplot as plt

img =cv2.imread("car.jpg")
nValores=funcionNegativo()
for i in range( img.shape[0]):
    for j in range (img.shape[1]):
        img.itemset((i,j,0),nValores[img.item(i,j,0)]) # Con esto cambiamos el valor de un pixel
        img.itemset((i, j, 1), nValores[img.item(i, j, 1)])
        img.itemset((i, j, 2), nValores[img.item(i, j, 2)])
cv2.imwrite("Negativo.jpg",img)
imgorg =cv2.imread("car.jpg")
cv2.imshow('image negativa ',img)
cv2.imshow('image original',imgorg)

color =("b","g","r")
for i, canal in enumerate(color):
    hist1 = cv2.calcHist([img],[i],None,[256],[0,256])
    #plt.plot(hist1, color= canal)
    #plt.xlim([0,256])
plt.show()
for j, canal in enumerate(color):
    hist2 = cv2.calcHist([imgorg], [j], None, [256], [0, 256])
    #plt.plot(hist2, color= canal)
    #plt.xlim([0,256])
plt.subplot(2, 2, 1), plt.plot(hist1, color=canal)
plt.subplot(2, 2, 2), plt.plot(hist2, color=canal)

plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()