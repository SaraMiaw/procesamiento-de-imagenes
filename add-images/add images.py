
#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

#IMPORTAR LIBRERIAS
import cv2
import numpy as np
#LEER IMAGENES
img1 = cv2.imread("road.jpg")
img2 = cv2.imread("car.jpg")
#TRANSFORMAR A GRIS
img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
#IMPRIMIR DIMENSIONES
print(img1[0, 0])
print(img2[0, 0])
#FILTROS
weighted = cv2.addWeighted(img1, 0.3, img2, 0.7, 0)
ret, mask = cv2.threshold(img2_gray, 252, 255, cv2.THRESH_BINARY_INV)
#ADICION
sum = cv2.add(img2, img1)
#MOSTRAR
cv2.imshow("sum", sum)
cv2.imshow("threshold", mask)
cv2.imshow("img2gray", img2_gray)
cv2.imshow("weighted", weighted)
cv2.imshow("img1", img1)
cv2.imshow("img2", img2)
cv2.waitKey(0)
cv2.destroyAllWindows()