
#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import cv2
import sys
import numpy as np

#nome = str(sys.argv[1])
#leemos la imagen
image = cv2.imread("nickel.jpg")
gs_imagem = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#para invertir la imagen
imagem = (255-gs_imagem)
#guardamos la nueva imageb
cv2.imwrite("invertida.png", imagem)

#forma 2
#nditer itera sobre matrices
for x in np.nditer(gs_imagem, op_flags=['readwrite']):
    x = abs(x - 255)
#guarda una nueva imagen
cv2.imwrite("invertida2.png", imagem)


#forma 3
#es con la funcion bitwise que invierte cada bit en un array

imagem2 = cv2.bitwise_not(gs_imagem)
cv2.imwrite("invertida3.png", imagem2)


#Mostramos
cv2.imshow("normal",image)

cv2.imshow("invertida1",imagem)

cv2.waitKey(0)
cv2.destroyAllWindows()


