#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:https://opencv.org/

import cv2 as cv
import os
from tkinter import *
import tkinter as tk
from tkinter import filedialog,messagebox
import os

img =''

def browseimg():
    global img
    fln = filedialog.askopenfilename(initialdir=os.getcwd(),title="CARGAR IMAGEN",filetypes=(("JPG Image","*.jpg"),("PNG Image","*.png"),("All files","*.*")))
    t1.set(fln)
    img=cv.imread(fln,cv.IMREAD_UNCHANGED)
    w.set(img.shape[0])
    h.set(img.shape[1])

def previewimg():
    cv.imshow("IMAGEN ESCOGIDA",img)
    cv.waitKey(0)
    cv.destroyAllWindows()

def recalcular():
    p=int(perc.get())
    nanh=int(int(w.get())*p/100)
    nalt=int(int(h.get())*p/100)
    w.set(nanh)
    h.set(nalt)

def preredimg():
    nw=int(w.get())
    nh=int(h.get())
    img2=cv.resize(img,(nw,nh),interpolation=cv.INTER_AREA)
    cv.imshow("previsualizacion",img2)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save():
    #fln1=filedialog.asksaveasfilename(initialdir=os.getcwd(),title="Save Image",filetypes=(("JPG Image","*.jpg"),("PNG Image","*.png"),("All files","*.*")))
    fln1="new.jpg"
    nw = int(w.get())
    nh = int(h.get())
    img2 = cv.resize(img, (nw,nh), interpolation=cv.INTER_AREA)
    cv.imwrite(fln1,img2)
    messagebox.showinfo("Imagen guardada","La imagen se ha guardado en "+os.path.basename(fln1)+" satisfactoriamente")

root =Tk()
t1=StringVar()
w=StringVar()
h=StringVar()
perc=StringVar()

wrapper=LabelFrame(root,text="Cargar imagen")
wrapper.pack(fill="both",expand="Yes",padx=20,pady=20)

wrapper2=LabelFrame(root,text="Detalles de la imagen")
wrapper2.pack(fill="both",expand="Yes",padx=20,pady=20)

wrapper3=LabelFrame(root,text="Acciones")
wrapper3.pack(fill="both",expand="Yes",padx=20,pady=20)

lbl=Label(wrapper,text="Cargar imagen:")
lbl.pack(side=tk.LEFT,padx=10,pady=10)

ent=Entry(wrapper,textvariable=t1)
ent.pack(side=tk.LEFT,padx=10,pady=10)

btn=Button(wrapper,text="cargar",command=browseimg)
btn.pack(side=tk.LEFT,padx=10,pady=10)

btnp=Button(wrapper,text="PREVIW",command=previewimg)
btnp.pack(side=tk.LEFT,padx=10,pady=10)


lbl2=Label(wrapper2,text="DIMENSIONES")
lbl2.pack(side=tk.LEFT,padx=10,pady=10)

entancho=Entry(wrapper2,textvariable=w)
entancho.pack(side=tk.LEFT,padx=5,pady=10)

lbl3=Label(wrapper2,text="X")
lbl3.pack(side=tk.LEFT,padx=10,pady=10)

entaltura=Entry(wrapper2,textvariable=h)
entaltura.pack(side=tk.LEFT,padx=5,pady=10)

wrapper4=LabelFrame(root,text="Salvarpixel")
wrapper4.pack(fill="both",expand="Yes",padx=20,pady=20)

lbl3=Label(wrapper4,text="Porcentage")
lbl3.pack(side=tk.LEFT,padx=10,pady=10)

entaltura=Entry(wrapper4,textvariable=perc)
entaltura.pack(side=tk.LEFT,padx=5,pady=10)

btnrec=Button(wrapper4,text="Recalculate Dimension",command=recalcular)
btnrec.pack(side=tk.LEFT,padx=5,pady=10)

prvf=Button(wrapper3,text="Previsualizacion redimension",command=preredimg)
prvf.pack(side=tk.LEFT,padx=5,pady=10)


save=Button(wrapper3,text="Salvar imagen",command=save)
save.pack(side=tk.LEFT,padx=5,pady=10)

root.title("REDIMENCIONAR IMAGEN")
root.geometry("800x600")
root.mainloop()
